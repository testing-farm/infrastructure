---

marks:
  - public
  - pipeline

variables:
  ARCH: aarch64
  COMPOSE: Fedora-Rawhide-aarch64
  REQUEST_ID: 97d594f8-813c-40e8-a11b-52c98c65226d

pipeline: |
  hide-secrets
  rules-engine
  testing-farm-request --request-id {{ REQUEST_ID }}
  ansible
  artemis
  fedora-copr:copr
  guest-setup-testing-farm:guest-setup
  install-copr-build
  install-koji-build-execute
  install-repository
  git
  coldstore
  testing-farm-request-state-reporter
  test-scheduler-sti
  test-schedule-runner-sti
  test-scheduler-testing-farm
  test-schedule-runner
  test-schedule-report

checks:
  literal-strings:
    - "[testing-farm-request] Initialized with {{ REQUEST_ID }}"
    - "[artemis] Using Artemis API"
    - "[git] <RemoteGitRepository(clone_url=https://gitlab.com/testing-farm/tests, branch=not specified, ref=main)>"
    - "[testing-farm-request-state-reporter] pipeline complete"
    - "[test-scheduler-sti] cloning repo"
    - "[test-schedule-runner] running test schedule of 1 entries:"
    - "[test-schedule-runner] 1 entries pending:"
    - "[test-schedule-runner] 0 entries pending:"
    - "[test-schedule-report] Result of testing: PASSED"

  literal-patterns:
    - "[testing-farm-request] Connected to Testing Farm Service '{{ testing_farm_public_api_url }}', '{{ testing_farm_internal_api_url }}'"
    - "[coldstore] For the pipeline artifacts, see {{ artifacts_url_pattern }}/{{ REQUEST_ID }}"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest is being provisioned"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest is ready"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest has become alive"
    - "[artemis] [{{ artemis_guestname_pattern }}] Guest provisioned"
    - "[artemis] [{{ artemis_guestname_pattern }}] destroying guest"
    - "[artemis] [{{ artemis_guestname_pattern }}] successfully released"

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - CREATED
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - ""
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - GUEST_PROVISIONING
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - ""
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - GUEST_SETUP
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - RUNNING
          - OK
          - UNDEFINED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - CLEANUP
          - OK
          - PASSED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

    - table:
        -
          - "git-main{{ '[0-9a-z_]+' }}/sti/tests.yaml"
          - COMPLETE
          - OK
          - PASSED
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - "{{ ARCH }} {{ COMPOSE }} S-"
          - sti

  artifacts:
    - "guest-setup-{{ artemis_guestname_pattern }}"
    - "guest-setup-{{ artemis_guestname_pattern }}/guest-setup-output-pre-artifact-installation.txt"
    - "guest-setup-{{ artemis_guestname_pattern }}/guest-setup-output-post-artifact-installation.txt"
    - "citool-debug\\.txt"
    - "citool-debug\\.verbose\\.txt"
    - "results\\.xml"
