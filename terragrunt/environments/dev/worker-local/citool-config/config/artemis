[default]
api-url = {{ artemis_api_url }}
api-version = {{ artemis_api_version }}
ssh-options = UserKnownHostsFile=/dev/null, StrictHostKeyChecking=no
ssh-key = ${config_root}/id_rsa_artemis.decrypted
key = master-key
user-data-vars-template-file = ${config_root}/artemis-user-data.yaml
post-install-script = ${config_root}/post-install-scripts/cloud-init-root

# Provisioning a guest in AWS should not take more then 30 minutes (artemis needs to become quicker).
# Timeout increased to 1 hour to support AutoSD's use case (TFT-2281).
ready-timeout = 3600

# API call timeout can be quite large. We are supposed to survive *reasonably* long outages, covering short
# outages and redeployments, and give human maintainers time to intervene in the case of more serious matters.
# Also, the tick can be a bit bigger than 1 second, let's try with 5 seconds.
api-call-timeout = 1800
api-call-tick = 5

# In some cases, like with AWS slaves, it can happen the connection takes longer to the machines.
# These options set the connection timeout for the checks also.
boot-tick = 30
echo-tick = 30

# TODO: to get rid of this, we need to allow connection from the artemis cluster to the guests
skip-prepare-verify-ssh = true

# TFT-892
guest-logs-enable = yes

# Artemis log configuration
guest-logs-config = ${config_root}/artemis-log-config.yaml
