# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "17.6.1"
  constraints = ">= 16.5.0"
  hashes = [
    "h1:bKnsDu1PpzB3uiSyHGQo+Tfa8TB+aDM2Cbxz3j6YdO8=",
    "zh:0cfa09eebd86db00820628db6b714cc2bba12fa2609d4202bf7487a476649a81",
    "zh:20926b7157d1eab9f27c459b18bb1277d9d64a1cdc5120ccc200cf41e014138c",
    "zh:3c211e04ab79c40369ecb2e1618617eccd6f6908b40c97ee2770f6b5dbba099d",
    "zh:41f0445308f68396a811baf083906fccb81fedeb618141c3f85c8ac5b3fabc8a",
    "zh:52bb58bda614af54943a63647d785ad47da27a2989fb6a82d9b23ff5dc86e534",
    "zh:5f85e99d44ea8359f0d6ce36a5402d23d5d27897a16b019ac07e23e05b6435ce",
    "zh:6c387f8621f09049a5768087a029a870a4aa00a924b04f2794c770f2578e7978",
    "zh:7c73065e421df8b303f25fb8d7181be7ee0fc0440c770f13b96409be8d9e0f39",
    "zh:7e848a539aa63d41567c1b8bf4140d7583a66a1c0bfcaaecf84fea4d1ca5508d",
    "zh:81acbed59221bd44144935615045c495dfdf658c0849db111b09acdb92c85d20",
    "zh:99e5f796446de91bf7988da4836600778c24f0dd3f9230108f72af85082e7665",
    "zh:b65b2e91dce8db63d2d1f19729885e7dd95f1eabcb19a3a73bd481639ea47910",
    "zh:bdd32ea764d15087aac1a13d3169f0ea7b16e1eb2d6ea601c3fb2b8c63c537a1",
    "zh:ed1a1ea0790b255f4222af6570d2e4c5b946b6cec34ba4a5d478bc98e5047a46",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}
