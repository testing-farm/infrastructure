# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "17.0.0"
  constraints = ">= 16.5.0"
  hashes = [
    "h1:nS4Jd0onD5Y4sGxaRxn2wU89SSR40vxCD0XzrB4JdsE=",
    "zh:11f608061277f2108023623a9b3199efa6f1170b88ac1f6d1c58cd22419a97f5",
    "zh:20d23218dfe1e8b66389ef7b0d1dc0ece3219d951e6945f1817d81aa805d0a77",
    "zh:2b2416b317c402d58e2c646b7d0251e1287349042c3e50bc7aad9cbc72d53e58",
    "zh:2fad00847ac9203a0ecb5e333604ef4c8ad96dd2d29b171d6592e2760e55c1be",
    "zh:47ae08df47cf19a41b993a1e47cc832bde20c034a445f9b39e5277b24ea1e04f",
    "zh:6c9e234feaed3a370c13c1f6f629d47ed13bd6532933ddf02fd85300c8c605bc",
    "zh:7ee5937b5fae3c5a5e1e34de0c46440e90113403b40e5829a99917c92b74cfb2",
    "zh:81aa5f1b30a76c277b93e44a7aadc4a15e5c7c959e1b2a0ba6667a80f5a5d107",
    "zh:8ebe7e2a0a9bb61b10ada8ed1135f3da42ee0e74c22d47db5b452aa9fbf78270",
    "zh:9ee077916e7117b61d21c1d29348ebdb415852f2920a912b96633835e1c268c6",
    "zh:b0dd7f70fb4f995a5a757e8dca1846fbd7a003629c915f74645390aef1249567",
    "zh:b242baae96ad1a6fb462dce3f171a8f5ba39bf72cffa942ab86fc011905e9b4f",
    "zh:e7e5ed817bf7b4b5d61c31697221c0f50f0bb3ba28d288e17b0bdd71f6d4c45b",
    "zh:f2eb5351a97de287558e19756c92fdbaa83035bcab52c7e79249181976f8bf40",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}
