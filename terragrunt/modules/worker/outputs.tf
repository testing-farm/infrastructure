output "workers_ip_ranges" {
  value = local.workers_ip_ranges
}

output "workers_instance_ids" {
  value = local.workers_instance_ids
}
