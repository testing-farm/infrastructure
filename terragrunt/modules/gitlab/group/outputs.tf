output "group_id" {
  value       = gitlab_group.group.id
  description = "The ID of the group in GitLab."
}
