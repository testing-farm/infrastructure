output "id" {
  value       = gitlab_project.project.id
  description = "The ID of the project in GitLab."
}
